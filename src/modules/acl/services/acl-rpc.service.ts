import { HttpException, Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class ACLRPCService {
  constructor(
    @Inject('SERVICE_ACL') private readonly ACLService: ClientProxy,
  ) {}

  public async send(payload: any, pattern: { cmd: string }): Promise<any> {
    const result = this.ACLService.send<string>(pattern, payload);
    const resume = await lastValueFrom<any>(result);

    if (resume.statusCode != 200 && resume.statusCode != 201) {
      throw new HttpException(resume.message, resume.statusCode);
    }
    delete resume.statusCode;
    return resume;
  }
}
