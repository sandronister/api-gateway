import { Controller, Get, Param, UseInterceptors } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CASTokenInterceptor } from 'src/core/interceptor/cas-token.interceptor';
import { ACLBFFDTO } from '../../dto/acl-bff.dto';
import { ACLAccountCodeResponse } from './acl-account-code.response';
import { ACLaAccountCodeService } from './acl-account-code.service';

@Controller('account')
@ApiTags('Account')
export class ACLAccountCodeController {
  constructor(private readonly service: ACLaAccountCodeService) {}

  @Get('code/:casToken/:accountId')
  @UseInterceptors(CASTokenInterceptor)
  @ApiOperation({ summary: 'Return codes account' })
  @ApiResponse({
    status: 200,
    description: 'success',
    type: ACLAccountCodeResponse,
  })
  @ApiResponse({ status: 403, description: 'invalid token' })
  @ApiResponse({ status: 400, description: 'invalid account' })
  async execute(@Param() params: ACLBFFDTO): Promise<any> {
    return await this.service.execute(params);
  }
}
