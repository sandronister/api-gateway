import { ApiProperty } from '@nestjs/swagger';

export class ACLAccountCodeResponse {
  @ApiProperty({
    example:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiQWxsaW4iLCJjb2RlIjp7ImFsbGluIjozLCJidGciOjc4OSwidHJhbnNpZCI6OTUxNywiY3VzdG9tZXJJZCI6IjNiYjBiYTgxLTYzNGYtNDA5Ni1iODMxLTdlYjdmZDYyYzY4MyJ9LCJpYXQiOjE2NDE5MDAwNzAsImV4cCI6MTY0MTk4NjQ3MH0.jTXGM1Kl9ff-Mzyf37UIahavY2N1b6MhanvIlx8mSrA',
  })
  accessToken: string;
}
