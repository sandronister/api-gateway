import { Injectable } from '@nestjs/common';
import { RedisCacheService } from 'src/core/cache/redis-cache.service';
import { ACLBFFDTO } from '../../dto/acl-bff.dto';
import { ACLRPCService } from '../../services/acl-rpc.service';

@Injectable()
export class ACLaAccountCodeService {
  private readonly CMD = { cmd: 'acl-account-code' };
  private readonly pattern = 'ACCOUNT-CODE';

  constructor(
    private readonly rpcService: ACLRPCService,
    private readonly redisService: RedisCacheService,
  ) {}

  public async execute(params: ACLBFFDTO): Promise<any> {
    const keyChache = `${params.casToken}-${params.accountId}-${this.pattern}`;

    if (await this.redisService.has(keyChache)) {
      return await this.redisService.get(keyChache);
    }

    const result = await this.rpcService.send(params.accountId, this.CMD);

    await this.redisService.set(keyChache, result);

    return result;
  }
}
