import { ApiProperty } from '@nestjs/swagger';

export class ACLAccountViewResponse {
  @ApiProperty({
    example: [
      {
        account: '61bc8bbe90f13f278958b748',
        name: 'Ingredientes Online',
        code: {
          allin: 14933,
        },
        applications: [
          {
            appId: '61b8f5aef02acef41931bbc4',
            name: 'Allin CLI Teste',
            redirect: 'localhost:8080/dashboard',
          },
          {
            appId: '61b8f5cff02acef41931bbcf',
            name: 'BTG Mobile',
            redirect: 'http://localhost:8080/ddddd',
          },
          {
            appId: '61c99e925b25a9aa77db5d68',
            name: 'Allin CLI Teste',
            redirect: 'localhost:8080/dashboard',
          },
        ],
      },
      {
        account: '61c99fe15b25a9aa77db5dc8',
        name: 'Hotel Urbano',
        code: {
          allin: '8974',
          btg: '768',
        },
        applications: [
          {
            appId: '61c99f5d5b25a9aa77db5db1',
            name: 'Nave Mae HOMOL',
            redirect: 'http://homol-navemae.allin.com.br:30010/',
          },
        ],
      },
    ],
  })
  public result: Array<any>;
}
