import { Injectable } from '@nestjs/common';
import { RedisCacheService } from 'src/core/cache/redis-cache.service';
import { ResultsInterface } from '../../interface/results.interface';
import { ACLRPCService } from '../../services/acl-rpc.service';

@Injectable()
export class AclAccountViewService {
  private readonly CMD = { cmd: 'acl-account' };
  private readonly pattern = 'ACCOUNT';

  constructor(
    private readonly rpcService: ACLRPCService,
    private readonly redisService: RedisCacheService,
  ) {}

  public async do(casToken: string): Promise<any> {
    if (await this.redisService.has(`${casToken}-${this.pattern}`)) {
      return await this.redisService.get(`${casToken}-${this.pattern}`);
    }
    const token = await this.redisService.get(casToken);
    const result = await this.rpcService.send(token, this.CMD);

    await this.redisService.set(`${casToken}-${this.pattern}`, result);

    return result;
  }
}
