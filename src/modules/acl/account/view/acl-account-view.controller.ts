import { Controller, Get, Param, UseInterceptors } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CASTokenInterceptor } from 'src/core/interceptor/cas-token.interceptor';
import { ResultsInterface } from '../../interface/results.interface';
import { ACLAccountViewResponse } from './acl-account-view.response';
import { AclAccountViewService } from './acl-account-view.service';

@Controller('account')
@ApiTags('Account')
export class ACLAccountViewController {
  constructor(private readonly service: AclAccountViewService) {}

  @Get(':casToken')
  @UseInterceptors(CASTokenInterceptor)
  @ApiOperation({ summary: 'Return all accounts from user' })
  @ApiResponse({
    status: 200,
    description: 'success',
    type: ACLAccountViewResponse,
  })
  @ApiResponse({ status: 403, description: 'invalid token' })
  public async do(
    @Param('casToken') casToken: string,
  ): Promise<ResultsInterface> {
    return await this.service.do(casToken);
  }
}
