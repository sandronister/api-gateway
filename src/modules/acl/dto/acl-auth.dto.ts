import { ApiProperty } from '@nestjs/swagger';

export class ACLAuthDTO {
  @ApiProperty({
    required: true,
    example: 'e6eaa447-8528-41c9-8acf-a7b1a88c33e6ds',
  })
  casToken: string;

  @ApiProperty({
    required: true,
    example: '61b8f5aef02acef41931bbc4',
  })
  appId: string;

  @ApiProperty({
    required: true,
    example: '5f356af6df701c51adb7bc7d',
  })
  accountId: string;
}
