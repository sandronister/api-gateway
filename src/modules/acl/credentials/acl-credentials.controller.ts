import { Controller, Get, Param, UseInterceptors } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CASTokenInterceptor } from 'src/core/interceptor/cas-token.interceptor';
import { ACLAuthDTO } from '../dto/acl-auth.dto';
import { AccessTokenInterface } from '../interface/access-token.interface';
import { ACLCredentialsResponse } from './acl-credentials.response';
import { ACLCredentialsService } from './acl-credentials.service';

@Controller('user')
@ApiTags('User')
export class ACLCredentialsController {
  constructor(private readonly service: ACLCredentialsService) {}

  @Get('/auth/:casToken/:appId/:accountId')
  @UseInterceptors(CASTokenInterceptor)
  @ApiOperation({
    summary: 'Return credentials from user application from account',
  })
  @ApiResponse({
    status: 200,
    description: 'success',
    type: ACLCredentialsResponse,
  })
  @ApiResponse({ status: 403, description: 'invalid token' })
  public async getCredentials(
    @Param() params: ACLAuthDTO,
  ): Promise<AccessTokenInterface> {
    return await this.service.do(params);
  }
}
