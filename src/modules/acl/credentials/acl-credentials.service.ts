import { Injectable } from '@nestjs/common';
import { RedisCacheService } from '../../../core/cache/redis-cache.service';
import { ACLAuthDTO } from '../dto/acl-auth.dto';
import { AccessTokenInterface } from '../interface/access-token.interface';
import { ACLRPCService } from '../services/acl-rpc.service';

@Injectable()
export class ACLCredentialsService {
  private readonly CMD = { cmd: 'acl-credentials' };
  private readonly pattern = 'CREDENTIALS-APP';

  constructor(
    private readonly rpcService: ACLRPCService,
    private readonly redisService: RedisCacheService,
  ) {}

  public async do(params: ACLAuthDTO): Promise<AccessTokenInterface> {
    const key = `${params.casToken}-${params.appId}-${this.pattern}`;

    if (await this.redisService.has(key)) {
      return await this.redisService.get(key);
    }

    const token = await this.redisService.get(params.casToken);

    const result = await this.rpcService.send(
      Object.assign(token, params),
      this.CMD,
    );

    await this.redisService.set(key, result);
    return result;
  }
}
