import { Controller, Headers, Post } from '@nestjs/common';
import { ACLMigrationService } from './acl-migration.service';

@Controller('migration')
export class ACLMigrationController {
  constructor(private readonly service: ACLMigrationService) {}

  @Post()
  async execute(@Headers('x-access-token') token: string) {
    return await this.service.execute(token);
  }
}
