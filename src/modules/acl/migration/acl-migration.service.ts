import { HttpException, Injectable } from '@nestjs/common';
import { lastValueFrom } from 'rxjs';
import { ACLRPCService } from '../services/acl-rpc.service';

@Injectable()
export class ACLMigrationService {
  constructor(private readonly rpcService: ACLRPCService) {}

  private readonly CMD = { cmd: 'acl-migration' };

  async execute(payload: string): Promise<any> {
    const { result } = await this.rpcService.send(payload, this.CMD);
    return result;
  }
}
