import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { RedisCacheModule } from 'src/core/cache/redis-cache.module';
import { ACLAccountViewController } from './account/view/acl-account-view.controller';
import { AclAccountViewService } from './account/view/acl-account-view.service';
import { ACLMSConfig } from 'src/core/config/aclMS.config';
import { ACLRPCService } from './services/acl-rpc.service';
import { ACLCredentialsService } from './credentials/acl-credentials.service';
import { ACLCredentialsController } from './credentials/acl-credentials.controller';
import { ACLAccountCodeController } from './account/code/acl-account-code.controller';
import { ACLaAccountCodeService } from './account/code/acl-account-code.service';
import { ACLMigrationController } from './migration/acl-migration.controller';
import { ACLMigrationService } from './migration/acl-migration.service';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'SERVICE_ACL',
        transport: Transport.TCP,
        options: {
          host: ACLMSConfig.HOST,
          port: ACLMSConfig.PORT,
        },
      },
    ]),
    RedisCacheModule,
  ],
  controllers: [
    ACLAccountViewController,
    ACLAccountCodeController,
    ACLCredentialsController,
    ACLMigrationController,
  ],
  providers: [
    AclAccountViewService,
    ACLaAccountCodeService,
    ACLRPCService,
    ACLCredentialsService,
    ACLMigrationService,
  ],
})
export class AclModule {}
