import { Body, Controller, HttpCode, Post } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { ForgotDTO } from '../dto/forgot.dto';
import { UserForgotService } from './user-forgot.service';

@Controller('user')
@ApiTags('User')
export class UserForgotController {
  constructor(private readonly service: UserForgotService) {}

  @Post('forgot')
  @HttpCode(200)
  @ApiResponse({
    status: 200,
    description: 'Send e-mail',
  })
  @ApiOperation({ summary: 'Forgot Password' })
  @ApiResponse({ status: 400, description: 'Invalid email' })
  public async execute(@Body() body: ForgotDTO): Promise<any> {
    return await this.service.execute(body.email);
  }
}
