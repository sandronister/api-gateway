import { HttpException, Injectable } from '@nestjs/common';
import { UserRPCService } from '../services/user-rpc.service';

@Injectable()
export class UserForgotService {
  private readonly CMD = { cmd: 'user-forgot' };

  constructor(private readonly rpcService: UserRPCService) {}

  async execute(email: string): Promise<any> {
    if (!email) {
      throw new HttpException('invalid e-mail', 400);
    }
    return await this.rpcService.send({ email }, this.CMD);
  }
}
