import { Controller, Get, Param, UseInterceptors } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CASTokenInterceptor } from 'src/core/interceptor/cas-token.interceptor';
import { UserACLViewResponse } from './user-acl-view.response';
import { UserACLViewService } from './user-acl-view.service';

@Controller('acl')
@ApiTags('ACL')
export class UserACLviewController {
  constructor(private readonly service: UserACLViewService) {}

  @Get(':casToken')
  @UseInterceptors(CASTokenInterceptor)
  @ApiOperation({ summary: 'Return all access from user' })
  @ApiResponse({
    status: 200,
    description: 'success',
    type: UserACLViewResponse,
  })
  @ApiResponse({ status: 403, description: 'invalid token' })
  @ApiResponse({ status: 400, description: 'acl not found' })
  public async execute(@Param('casToken') casToken: string): Promise<any> {
    return await this.service.execute(casToken);
  }
}
