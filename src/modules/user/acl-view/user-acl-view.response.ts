import { ApiProperty } from '@nestjs/swagger';

export class UserACLViewResponse {
  @ApiProperty({
    example: [
      {
        permissions: [
          'template.crud',
          'template.download',
          'report.download',
          'report.create',
          'report.read',
          'switchAccount.read',
          'notificationPanel.read',
        ],
      },
    ],
  })
  public result: Array<any>;
}
