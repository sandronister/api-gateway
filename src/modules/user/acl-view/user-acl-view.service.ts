import { Injectable } from '@nestjs/common';
import { RedisCacheService } from 'src/core/cache/redis-cache.service';
import { UserRPCService } from '../services/user-rpc.service';

@Injectable()
export class UserACLViewService {
  private readonly CMD = { cmd: 'acl-user' };
  private readonly pattern = 'userACLView';

  constructor(
    private readonly rpcService: UserRPCService,
    private readonly redisService: RedisCacheService,
  ) {}

  public async execute(casToken: string): Promise<any> {
    if (await this.redisService.has(`${casToken}-${this.pattern}`)) {
      return await this.redisService.get(`${casToken}-${this.pattern}`);
    }

    const token = await this.redisService.get(casToken);
    const result = await this.rpcService.send(token, this.CMD);

    await this.redisService.set(`${casToken}-${this.pattern}`, result);

    return result;
  }
}
