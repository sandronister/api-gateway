import { HttpException, Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { x } from 'joi';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class UserRPCService {
  constructor(
    @Inject('SERVICE_AUTH') private readonly authService: ClientProxy,
  ) {}

  async send(payload: any, pattern: { cmd: string }): Promise<any> {
    const result = this.authService.send<string>(pattern, payload);
    const resume = await lastValueFrom<any>(result);
    if (resume.statusCode != 200 && resume.statusCode != 201) {
      throw new HttpException(resume.message, resume.statusCode);
    }
    delete resume.statusCode;
    return resume;
  }
}
