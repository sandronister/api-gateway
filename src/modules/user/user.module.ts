import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { RedisCacheModule } from 'src/core/cache/redis-cache.module';
import { UserLoginController } from './login/user-login.controller';
import { UserRenewController } from './renew/user-renew.controller';
import { UserLoginService } from './login/user-login.service';
import { UserRenewService } from './renew/user-renew.service';
import { UserAuthService } from './auth/user-auth.service';
import { UserAuthController } from './auth/user-auth.controller';
import { AuthMSConfig } from 'src/core/config/authMS.config';
import { UserRPCService } from './services/user-rpc.service';
import { UserACLViewService } from './acl-view/user-acl-view.service';
import { UserACLviewController } from './acl-view/user-acl-view.controller';
import { UserForgotController } from './forgot/user-forgot.controller';
import { UserForgotService } from './forgot/user-forgot.service';
import { UserAlterController } from './alter/user-alter.controller';
import { UserAlterService } from './alter/user-alter.service';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'SERVICE_AUTH',
        transport: Transport.TCP,
        options: {
          host: AuthMSConfig.HOST,
          port: AuthMSConfig.PORT,
        },
      },
    ]),
    RedisCacheModule,
  ],
  controllers: [
    UserLoginController,
    UserRenewController,
    UserAuthController,
    UserACLviewController,
    UserForgotController,
    UserAlterController,
  ],
  providers: [
    UserLoginService,
    UserRenewService,
    UserAuthService,
    UserACLViewService,
    UserForgotService,
    UserAlterService,
    UserRPCService,
  ],
})
export class UserModule {}
