import { ApiProperty } from '@nestjs/swagger';

export class UserLoginResponse {
  @ApiProperty({
    example: 'afe42caa-851a-4959-ac07-70b05af29d83',
  })
  casToken: string;
}
