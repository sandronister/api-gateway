import { Injectable } from '@nestjs/common';
import { RedisCacheService } from 'src/core/cache/redis-cache.service';
import { v4 } from 'uuid';
import { UserDTO } from '../dto/user.dto';
import { CASTokenInterface } from '../interface/cas-token.interface';
import { UserRPCService } from '../services/user-rpc.service';

@Injectable()
export class UserLoginService {
  private readonly CMD = { cmd: 'login' };

  constructor(
    private readonly rpcService: UserRPCService,
    private readonly redisService: RedisCacheService,
  ) {}

  async do(payload: UserDTO): Promise<CASTokenInterface> {
    const token = await this.rpcService.send(payload, this.CMD);
    console.log(token, '----------------------------');
    const hash = v4();
    await this.redisService.set(hash, token);
    return { casToken: hash };
  }
}
