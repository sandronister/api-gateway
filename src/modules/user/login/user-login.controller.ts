import { Body, Controller, HttpCode, Post } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { UserDTO } from '../dto/user.dto';
import { CASTokenInterface } from '../interface/cas-token.interface';
import { UserLoginResponse } from './user-login.response';
import { UserLoginService } from './user-login.service';

@Controller('user')
@ApiTags('User')
export class UserLoginController {
  constructor(private readonly service: UserLoginService) {}

  @Post('login')
  @HttpCode(200)
  @ApiResponse({
    status: 200,
    description: 'success',
    type: UserLoginResponse,
  })
  @ApiOperation({ summary: 'User Login' })
  @ApiResponse({ status: 403, description: 'invalid user or password' })
  @ApiResponse({ status: 401, description: 'blocked User' })
  async handle(@Body() body: UserDTO): Promise<CASTokenInterface> {
    return await this.service.do(body);
  }
}
