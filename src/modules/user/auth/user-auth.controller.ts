import { Controller, Get, Param, UseInterceptors } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CASTokenInterceptor } from 'src/core/interceptor/cas-token.interceptor';
import { UserAuthResponse } from './user-auth.reponse';
import { UserAuthService } from './user-auth.service';

@Controller('user')
@ApiTags('User')
export class UserAuthController {
  constructor(private readonly service: UserAuthService) {}

  @Get('auth/:casToken')
  @UseInterceptors(CASTokenInterceptor)
  @ApiOperation({ summary: 'Get JWT Token' })
  @ApiResponse({ status: 200, description: 'success', type: UserAuthResponse })
  @ApiResponse({ status: 403, description: 'invalid token' })
  handle(@Param('casToken') casToken: string): Promise<string> {
    return this.service.do(casToken);
  }
}
