import { HttpException, Injectable } from '@nestjs/common';
import { RedisCacheService } from 'src/core/cache/redis-cache.service';

@Injectable()
export class UserAuthService {
  constructor(private readonly redisService: RedisCacheService) {}

  async do(payload: string): Promise<string> {
    const token = await this.redisService.get(payload);

    if (!token.hasOwnProperty('accessToken')) {
      throw new HttpException('Invalid token', 403);
    }

    return token;
  }
}
