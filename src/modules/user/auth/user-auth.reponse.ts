import { ApiProperty } from '@nestjs/swagger';

export class UserAuthResponse {
  @ApiProperty({
    example:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MWIzNGEzY2YwMmFjZWY0MTkzMTkyMzIiLCJuYW1lIjoiTmFkaWEgTWFjaWVsIiwiY29tcGFueSI6IkFsbGluICYgU29jaWFsIiwidXNlcm5hbWUiOiJuYWRpYS5tYWNpZWwiLCJlbWFpbCI6Im5hZGlhLm1hY2llbEBhbGxpbi5jb20uYnIiLCJsYW5ndWFnZSI6InB0QlIiLCJpc0NsaWVudCI6ZmFsc2UsInN0YXR1cyI6dHJ1ZSwiaWF0IjoxNjM5NjgyNDA5LCJleHAiOjE2Mzk3Njg4MDl9.Ut0t_ZTiQZ7ONcU4JT7XPPrRdIE3e7MQtyrAdWGcZfQ',
  })
  accessToken: string;
}
