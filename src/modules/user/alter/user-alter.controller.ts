import { Body, Controller, Headers, HttpCode, Put } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { UpdatePassDTO } from '../dto/updatePass.dto';
import { UserAlterService } from './user-alter.service';

@Controller('user')
@ApiTags('User')
export class UserAlterController {
  constructor(private readonly service: UserAlterService) {}

  @Put()
  @HttpCode(204)
  @ApiResponse({
    status: 204,
    description: 'Success',
  })
  @ApiOperation({ summary: 'Update password' })
  @ApiResponse({ status: 400, description: 'Invalid email' })
  async execute(
    @Body() body: UpdatePassDTO,
    @Headers('authorization') authorization: string,
  ): Promise<any> {
    const { newPass, confirm } = body;
    return await this.service.execute(authorization, newPass, confirm);
  }
}
