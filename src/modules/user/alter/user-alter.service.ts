import { HttpException, Injectable } from '@nestjs/common';
import { UserRPCService } from '../services/user-rpc.service';

@Injectable()
export class UserAlterService {
  private readonly CMD = { cmd: 'user-alter' };

  constructor(private readonly rpcService: UserRPCService) {}

  async execute(
    accessToken: string,
    newPass: string,
    confirm: string,
  ): Promise<any> {
    if (!accessToken) {
      throw new HttpException('invalid token initial', 401);
    }
    return await this.rpcService.send(
      { accessToken, newPass, confirm },
      this.CMD,
    );
  }
}
