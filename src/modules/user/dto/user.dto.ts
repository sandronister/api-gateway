import { ApiProperty } from '@nestjs/swagger';

export class UserDTO {
  @ApiProperty({ required: false, example: 'joao.silva' })
  login: string;

  @ApiProperty({ required: true, example: '1234' })
  password: string;
}
