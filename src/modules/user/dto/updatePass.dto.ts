import { ApiProperty } from '@nestjs/swagger';

export class UpdatePassDTO {
  @ApiProperty({ required: true, example: 'tfasfu' })
  newPass: string;

  @ApiProperty({ required: true, example: 'tfasfu' })
  confirm: string;
}
