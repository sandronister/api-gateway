import { ApiProperty } from '@nestjs/swagger';

export class ForgotDTO {
  @ApiProperty({ required: true, example: 'joao@allin.com.br' })
  email: string;
}
