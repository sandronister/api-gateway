import { HttpException, Injectable } from '@nestjs/common';
import { RedisCacheService } from 'src/core/cache/redis-cache.service';
import { v4 } from 'uuid';
import { CASTokenInterface } from '../interface/cas-token.interface';
import { UserRPCService } from '../services/user-rpc.service';

@Injectable()
export class UserRenewService {
  private readonly CMD = { cmd: 'renew' };

  constructor(
    private readonly rpcService: UserRPCService,
    private readonly redisService: RedisCacheService,
  ) {}

  async do(payload: string): Promise<CASTokenInterface> {
    const token = await this.rpcService.send(payload, this.CMD);
    const hash = v4();
    await this.redisService.set(hash, token);
    return { casToken: hash };
  }
}
