import { Controller, Get, Headers } from '@nestjs/common';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CASTokenInterface } from '../interface/cas-token.interface';
import { UserLoginResponse } from '../login/user-login.response';
import { UserRenewService } from './user-renew.service';

@Controller('user')
@ApiTags('User')
export class UserRenewController {
  constructor(private readonly service: UserRenewService) {}

  @Get('renew')
  @ApiOperation({ summary: 'Renew JWT Token' })
  @ApiResponse({ status: 200, description: 'success', type: UserLoginResponse })
  @ApiResponse({ status: 403, description: 'invalid token' })
  async do(
    @Headers('authorization') authorization: string,
  ): Promise<CASTokenInterface> {
    return await this.service.do(authorization);
  }
}
