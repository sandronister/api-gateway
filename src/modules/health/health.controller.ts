import { Controller, Get, HttpCode } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

@Controller('health')
export class HealthController {
  @Get()
  @HttpCode(200)
  @ApiOperation({ summary: 'Return server is ok' })
  @ApiResponse({
    status: 200,
    description: 'success',
  })
  @ApiTags('Health Check')
  health(): string {
    return 'Alive!';
  }
}
