import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { RedisCacheModule } from 'src/core/cache/redis-cache.module';
import { AuthMSConfig } from 'src/core/config/authMS.config';
import { ApplicationRPCService } from './services/application-rpc.service';
import { ApplicationViewController } from './view/application-view.controller';
import { ApplicationViewService } from './view/application-view.service';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'SERVICE_AUTH',
        transport: Transport.TCP,
        options: {
          host: AuthMSConfig.HOST,
          port: AuthMSConfig.PORT,
        },
      },
    ]),
    RedisCacheModule,
  ],
  controllers: [ApplicationViewController],
  providers: [ApplicationViewService, ApplicationRPCService],
})
export class ApplicationModule {}
