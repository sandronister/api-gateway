import { HttpException, Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class ApplicationRPCService {
  constructor(
    @Inject('SERVICE_AUTH') private readonly authService: ClientProxy,
  ) {}

  async send(payload: any, pattern: { cmd: string }): Promise<any> {
    const result = this.authService.send<string>(pattern, payload);
    const resume = await lastValueFrom<any>(result);

    if (resume.statusCode != 200) {
      throw new HttpException(resume.message, resume.statusCode);
    }
    delete resume.statusCode;
    return resume;
  }
}
