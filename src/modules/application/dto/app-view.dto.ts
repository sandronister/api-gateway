import { ApiProperty } from '@nestjs/swagger';

export class AppViewDTO {
  @ApiProperty({
    required: true,
    example: '248ca03d-ba63-4564-8ae5-2fdb2a6a479b',
  })
  casToken: string;

  @ApiProperty({ required: true, example: '71b8f5cff02acef41931bbcf' })
  appId: string;
}
