import { Controller, Get, Param, UseInterceptors } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CASTokenInterceptor } from 'src/core/interceptor/cas-token.interceptor';
import { AppViewDTO } from '../dto/app-view.dto';
import { AppResponseInterface } from '../interface/app-response.interface';
import { ApplicationViewResponse } from './application-view.response';
import { ApplicationViewService } from './application-view.service';

@Controller('application')
export class ApplicationViewController {
  constructor(private readonly service: ApplicationViewService) {}

  @Get(':casToken/:appId')
  @UseInterceptors(CASTokenInterceptor)
  @ApiTags('Application')
  @ApiOperation({ summary: 'Return redirect from application' })
  @ApiResponse({
    status: 200,
    description: 'success',
    type: ApplicationViewResponse,
  })
  @ApiResponse({ status: 403, description: 'invalid token' })
  @ApiResponse({ status: 400, description: 'application not found' })
  public async execute(
    @Param() param: AppViewDTO,
  ): Promise<AppResponseInterface> {
    return await this.service.execute(param);
  }
}
