import { HttpException, Injectable } from '@nestjs/common';
import { RedisCacheService } from 'src/core/cache/redis-cache.service';
import { AppViewDTO } from '../dto/app-view.dto';
import { AppResponseInterface } from '../interface/app-response.interface';
import { ApplicationRPCService } from '../services/application-rpc.service';

@Injectable()
export class ApplicationViewService {
  private readonly CMD = { cmd: 'view-app' };
  constructor(
    private readonly rpcService: ApplicationRPCService,
    private readonly redisService: RedisCacheService,
  ) {}

  public async execute(param: AppViewDTO): Promise<AppResponseInterface> {
    return await this.rpcService.send(param.appId, this.CMD);
  }
}
