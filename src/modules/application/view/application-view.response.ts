import { ApiProperty } from '@nestjs/swagger';

export class ApplicationViewResponse {
  @ApiProperty({ example: 'https:\\local.allin.com.br' })
  redirect: string;
}
