import { Module } from '@nestjs/common';
import { CoreConfigModule } from './core/config/coreConfig.module';
import { RedisCacheModule } from './core/cache/redis-cache.module';
import { HealthModule } from './modules/health/health.module';
import { UserModule } from './modules/user/user.module';
import { AclModule } from './modules/acl/acl.module';
import { ApplicationModule } from './modules/application/application.module';

@Module({
  imports: [
    CoreConfigModule,
    RedisCacheModule,
    ApplicationModule,
    AclModule,
    UserModule,
    HealthModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
