import { RedisCacheService } from './redis-cache.service';
import { Module, CacheModule } from '@nestjs/common';
import * as redisSttore from 'cache-manager-redis-store';
import { RedisConfig } from '../config/redis.config';


@Module({
    imports: [
        CacheModule.registerAsync({
            useFactory: async () => ({
                store: redisSttore,
                host: RedisConfig.HOST,
                port: RedisConfig.PORT,
                ttl: RedisConfig.TTL
            })
        })
    ],
    controllers: [],
    providers: [
        RedisCacheService,],
    exports: [RedisCacheService]
})
export class RedisCacheModule { }
