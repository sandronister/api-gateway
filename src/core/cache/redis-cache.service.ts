import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';
import { Cache } from 'cache-manager';

@Injectable()
export class RedisCacheService {
  constructor(@Inject(CACHE_MANAGER) private readonly cache: Cache) {}

  async get(key): Promise<any> {
    return await this.cache.get(key);
  }

  async set(key, value): Promise<void> {
    await this.cache.set(key, value);
  }

  async has(key: string): Promise<boolean> {
    const val = await this.cache.get(key);
    return val != null;
  }
}
