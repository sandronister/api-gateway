import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  HttpException,
} from '@nestjs/common';
import { lastValueFrom, Observable } from 'rxjs';
import { RedisCacheService } from '../cache/redis-cache.service';

@Injectable()
export class CASTokenInterceptor implements NestInterceptor {
  constructor(private readonly redisService: RedisCacheService) {}

  async intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Promise<Observable<any>> {
    const args = context.getArgs();

    if (!(await this.redisService.has(args[0].params.casToken))) {
      throw new HttpException('Invalid token', 403);
    }

    return await lastValueFrom(next.handle());
  }
}
