const { env } = process;

export const variables = (): any => {
  return {
    NODE_ENV: env.NODE_ENV,
    HTTP_PORT: env.HTTP_PORT,
    REDIS_HOST: env.REDIS_HOST,
    REDIS_PORT: env.REDIS_PORT,
    REDIS_TTL: env.REDIS_TTL,
    AUTHMS_HOST: env.AUTHMS_HOST,
    AUTHMS_PORT: env.AUTHMS_PORT,
    ACLMS_HOST: env.ACLMS_HOST,
    ACLMS_PORT: env.ACLMS_PORT,
  };
};
