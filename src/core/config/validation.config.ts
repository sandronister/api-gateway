import * as Joi from 'joi';

export const validationSchema = Joi.object({
  NODE_ENV: Joi.string().required(),
  HTTP_PORT: Joi.number().required(),
  REDIS_HOST: Joi.string().required(),
  REDIS_PORT: Joi.number().required(),
  REDIS_TTL: Joi.number().required(),
  AUTHMS_HOST: Joi.string().required(),
  AUTHMS_PORT: Joi.number().required(),
  ACLMS_HOST: Joi.string().required(),
  ACLMS_PORT: Joi.number().required(),
});
