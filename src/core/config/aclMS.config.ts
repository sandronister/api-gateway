import { ConfigService } from '@nestjs/config';

const configService = new ConfigService();

export class ACLMSConfig {
  public static readonly HOST = configService.get('ACLMS_HOST');
  public static readonly PORT = configService.get('ACLMS_PORT');
}
