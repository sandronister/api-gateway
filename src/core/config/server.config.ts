import { ConfigService } from '@nestjs/config';

const configService = new ConfigService();

export class ServerConfig {
  public static readonly HTTP_PORT = configService.get('HTTP_PORT');
}
