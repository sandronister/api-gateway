import { ConfigService } from "@nestjs/config"

const configService = new ConfigService()

export class RedisConfig {
    static readonly HOST = configService.get('REDIS_HOST')
    static readonly PORT = configService.get('REDIS_PORT')
    static readonly TTL = configService.get('REDIS_TTL')
}