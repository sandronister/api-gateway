import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { variables } from './variables.config';
import { validationSchema } from './validation.config';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [variables],
      validationSchema,
    }),
  ],
  controllers: [],
  providers: [],
})
export class CoreConfigModule { }

