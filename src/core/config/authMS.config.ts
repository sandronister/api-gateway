import { ConfigService } from "@nestjs/config";

const configService = new ConfigService();

export class AuthMSConfig {
    public static readonly HOST = configService.get('AUTHMS_HOST')
    public static readonly PORT = configService.get('AUTHMS_PORT')
}